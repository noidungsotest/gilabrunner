echo 'deploy app to server PROD: =======>'
rm -fr $HOME/gilabrunner
mkdir -p $HOME/gilabrunner
cd $HOME/gilabrunner
git clone https://gitlab.com/noidungsotest/gilabrunner.git .

docker stack deploy --compose-file docker-compose-prod.yml stackpython
echo '=====> deploy success on PROD server'
